"""
First input = Number of line
Second input = some word separate by space
...
...
Output is a number

Ex :
3
yep pmr
pmr momr drbrm
yep xrtp gobr momr

Answer :
111222111

Ex :
2
yep momr
momr rohjy drbrm

Answer :
99999
"""

number_of_line = int(input())
line_input = []

i = 0
while i < number_of_line:
    line_input.append(input())
    i += 1

text_to_int = {
    "xrtp": 0,
    "pmr": 1,
    "yep": 2,
    "yjtrr": 3,
    "gpit": 4,
    "gobr": 5,
    "doc": 6,
    "drbrm": 7,
    "rohjy": 8,
    "momr": 9,
}

str_list = []
tmp_str = ""
for line in line_input:
    tmp_list = line.split()
    for value in tmp_list:
        tmp_str += str(text_to_int[value])

    str_list.append(tmp_str)
    tmp_str = ""

int_list = []
tmp_value = 0

for value in str_list:
    tmp_value = 0
    for index, letter in enumerate(value[::-1]):
        tmp_value += (int(letter) * 16 ** index)
    int_list.append(tmp_value)

result = 1
for value in int_list:
    result = result * value

print(result)
