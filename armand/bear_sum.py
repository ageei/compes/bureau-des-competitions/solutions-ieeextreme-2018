"""
Bear Sums
Time limit: 22500 ms
Memory limit: 256 MB

Mitsos the bear is challenging his brother Vangelis with a mathematical task. Mitsos provides a list of integers LL and another integer value SS, then he asks Vangelis to check if there are any two items in the list LL whose sum is equal to the integer SS.

Since Vangelis is a confident engineer, he decided to write a program that would do all the computations for him and skip the trouble of thinking. Your task is to help Vangelis write a program that reads the input list LL and the integer SS and produces either the solution to the problem or provides an error message when there is no solution available.

Standard input
On the first line there will be an integer number TT (representing the number of test cases) followed by 22 input lines for each test case:

On the first line of each test case, there will be 22 integers SS and EE, where SS is the expected sum and EE is the number of elements in the list.
On the second line, there will be EE integers separated by a space. Each integer represents an element of the list LL. The elements are not sorted in any way and some could have the same value. In cases where the number EE is 00, the second line will be empty.
All values for the elements of list LL will be in the same range as the value SS.

Standard output
For each test case you will have to write one line that contains:

If there is an unique solution: Write two elements, xx and yy of the list LL, separated by a single space, such that x + y = Sx+y=S and x \le yx≤y.
If there are multiple solutions: Pick the first complete pair that appears on the list and provides the correct sum. Print the two list elements forming this pair in increasing order, as above.
If there is no solution: Print the error message !OK.
Constraints and notes
1 \leq T \leq 1\ 0001≤T≤1 000
-10^6 < S < 10^6−10
​6
​​ <S<10
​6
​​
0 \leq E \leq 2 \cdot 10^40≤E≤2⋅10
​4
​​
The sum of values of EE is at most 10^710
​7
​​
Ex:
6
8 4
1 2 4 4
8 4
1 2 7 9
8 4
1 2 8 9
8 4
4 5 3 4
8 4
4 1 1 8
8 4
-1 1 9 8

Output:
4 4
1 7
!OK
3 5
!OK
-1 9
"""


def found_couple(target, data_list):
    input_list = set()
    complementary_list = set()
    target = int(target)
    couple = []

    for value in data_list:
        if (target - value) in input_list:
            if target - value < value:
                couple = [target - value, value]
                break
            else:
                couple = [value, target - value]
                break
        complementary_list.add(target - value)
        input_list.add(value)

    return couple


number_of_test = input()
couples = []

i = 0
while i < int(number_of_test):
    test_first_input = input()
    test_first_input_split = [int(n) for n in test_first_input.split()]

    test_second_input = input()
    test_second_input_split = [int(n) for n in test_second_input.split()]
    search = found_couple(test_first_input_split[0], test_second_input_split)
    if search:
        couples.append("{} {}".format(search[0], search[1]))
    else:
        couples.append("!OK")

    i += 1

for couple in couples:
    print(couple)

