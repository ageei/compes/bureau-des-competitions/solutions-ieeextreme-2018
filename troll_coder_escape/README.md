# Troll Coder Escape

Énoncé: https://csacademy.com/ieeextreme12/task/troll-coder-escape/

The treasure is in your hands but there is only one way out of the island: the same way you used to get in! You have to cross the bridge again and the Troll is there waiting for you.

You are now faced with the same sequence guessing game, each sequence with exactly *N=100*. You have to solve every test with a maximum of *N+1* queries to successfully cross the bridge, but the more queries you use the more treasure you will lose to the Troll.

You final score for this challenge will be based on the average number of queries needed per test case. If you use *N+1* queries on all the tests you will not score any points.

## Interaction

At the beginning, you must read *T*, the number of test cases.

At the beginning of each test case, the Troll will give you a single integer *N* which will represent the length of the sequence.

To submit a query, your program should output the letter Q followed by a space and by a binary sequence of length *N* with each bit separated by a space.
After each query you will receive an integer denoting the number of correct bits.
The last submission will be your final answer and it should start with an A followed by a space and by a binary sequence of length *N* with each bit separated by a space.

## Scoring

Assuming you provided a correct solution for all test cases, the scoring is computed as follows:
let *A* be the average of number of Q queries over all cases, then your score will be $`5 \cdot (80-A)`$.
In case this is negative, you will score *0* points. In case this is greater than *100*, you will score *100* points.

## Constraints and notes

- This task is NOT adaptive
- You will be scored based on a single run of $`T=100`$, with $`N=100`$ for each test case.
- our program can submit at most $`N+1`$ queries before arriving at the correct answer.

## Example

|Input|Output|Explanation|
|-----|------|-----------|
|2<br>6|| For clarity, the example uses N=6, but the actual test cases will consist solely of N=100.|
||`Q 0 0 0 0 0 0`| Transform into 5 4 3 2.|
|2|||
||`A 1 0 1 1 0 1`||
|6|||
||`A 0 0 1 0 1 1`||

## Solution

Utilisons N=6 pour simplifier le processus.  
La combinaison secrète est `101001`.
> Supposition initiale :  
> `Q 0 0 0 0 0 0`  
Réponse :  
> `3`  

On sait donc que le nombre de `0` dans la réponse est 3.  
On ne conserve que les permutations de 6 bits qui contiennent un nombre de `0` égal à la réponse du serveur (`3`):  
> int -> [7, 11, 13, 14, ..., 56]  
> bin -> [000111, 001011, 001101, 001110, ..., 111000]

Ensuite, tant que le serveur renvois un nombre plus bas que 6, on tente une supposition avec une permutation de bits dans celles conservées. Prenons la plus petite valeur (`7` / `000111`) :  

> Prochaine supposition :  
> `Q 0 0 0 1 1 1`  
Réponse :  
> `2`

Grâce à la nouvelle réponse, on calcule le delta du nombre de bits corrects :
```python
# whole -> supposition courante (000111)
# correct_bits_for(whole) -> réponse du serveur pour la supposition courante (2)
# whole_ok_bits -> réponse du serveur pour la supposition précédente (3)
delta = correct_bits_for(whole) - whole_ok_bits # delta = 2 - 3 = -1
```

### Information sur les flips

L'étape suivant consiste à établir un `flip_mask`, c'est à dire une séquence binaire qui aura un `1` seulement pour les bits qui ont changé (flips) entre les deux derniers essais. Dans cet exemple, on a `last_attempt = 000000` et `attempt = 000111`, on fait donc un ou-exclusif (XOR `^`) entre ces deux valeur pour trouver le `flip_mask` :

```python
flip_mask = last_attempt ^ attempt # 000111 = 000000 ^ 000111
```

On trouve ensuite le nombre de 'flips' total, qui correspond au nombre de `1` dans `flip_mask` et le nombre de 'bad flips' qui correspond à $`\lfloor(num\_flips - delta)/2\rfloor`$. En effet, si `delta == num_flips`, tous les flips sont bons et `bad_flips = 0`, et 'a l'inverse, si `delta == -num_flips`, alors tous les flips sont mauvais :

```python
num_flips = number_of_set_bits(flip_mask) # num_flips = 3
bad_flips = (num_flips - delta) // 2 # bad_flips = (3 - (-1)) // 2 = 2
```

### Condition sur les permutations

On veut ensuite conserver uniquement les permutations de bits qui peuvent satisfaire cette réponse du serveur, c'est-à-dire qui contiennent exactement 2 bits `0` et 1 bit `1` spécifiquement dans les positions ou il y a eut un flip. Ceci est le cas puique dans notre exemple, on flip `000` vers `111` avec 2 bad flip, donc 2 positions qui ne continnent pas de `1`.

### Générer les masques de comparaison

On commence par générer tous les masques de longueur égale à notre `flip_mask` avec un nombre de `1` égal au nombre de bad flips (ici 2).
```python
# xor_masks = [000011, 000101, 000110, ..., 110000]
xor_masks = compute_bit_masks(flip_mask.bit_length(), bad_flips)
```
Puis, pour chaque `xor_mask`, on remet à `0` les bits qui ne correspondent pas aux positions flippées :
```python
# (flip_mask ^ xor_mask) & flip_mask = (111 ^ xor_mask) & 111 = ???
unfiltered_comp_masks = [
    (flip_mask ^ xor_mask) & flip_mask for xor_mask in xor_masks]
# unfiltered_comp_masks = [100, 010, 001, ..., 111]
```
Ensuite, on élimine les masques non utile, c'est-à-dire ceux qui ne contiennent plus un nombre de `1` égal au nombre de good flips (`num_flips - bad_flips`) (On se rappelle que `& flip_mask` a éliminé plusieurs `1` dans les `xor_masks`) :

```python
# num_flips - bad_flips = 3 - 2 = 1
def comp_filter(m): return number_of_set_bits(m) == num_flips - bad_flips
comp_masks = list(filter(comp_filter, unfiltered_comp_masks))
# comp_masks = [100, 010, 001]
```

### Comparaison des permutations avec les masques

Finalement, on compare les permutations de bits restantes avec chaque `comp_mask`. Si au moins un masque satisfait la comparaison, alors cette permutation est acceptable et on la conserve :
```python 
new_possibilities = []
# possibilities[i][(len(part), part_1s, 0)] -> Toutes les permutations restantes
for num in possibilities[i][(len(part), part_1s, 0)]:
    found = False
    # Comparer la permutation (num) avec tous les masques
    for comp_mask in comp_masks:
        # & flip_mask permet d'ignorer les bits non flippées
        if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask:
            found = True
            break
    if found:
        new_possibilities.append(num)
last_attempt = attempt
```
Pour le code ci-haut, prenons les permutations établies précédemment :
> int -> [7, 11, 13, 14, ..., 56]  
> bin -> [000111, 001011, 001101, 001110, ..., 111000]

#### Essayons avec `num = 11` (`001011`)

```python
# attempt = 111 (7)
# comp_masks = [100, 010, 001]
# gauche -> (attempt ^ (~comp_mask)) & flip_mask
# droite -> num & flip_mask
for comp_mask in comp_masks:
        # gauche -> (attempt ^ (~comp_mask)) & flip_mask
        # droite -> num & flip_mask
        if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask:
            found = True
            break
```
Pour le premier comp_mask `100`:

```python
# gauche : 111 ^ ~100 & 111 = 111 ^ 011 & 111 = 100
# droite : 1011 & 111 = 011
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # false
    found = True
    break
```

Pour le deuxième comp_mask `010`:

```python
# gauche : 111 ^ ~010 & 111 = 111 ^ 101 & 111 = 010
# droite : 1011 & 111 = 011
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # false
    found = True
    break
```

Pour le troisième comp_mask `001`:

```python
# gauche : 111 ^ ~001 & 111 = 111 ^ 110 & 111 = 001
# droite : 1011 & 111 = 011
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # false
    found = True
    break
```

> `11` n'est donc pas possible et doit être éliminé.

#### Essayons avec `num = 25` (`011001`)

Pour le premier comp_mask `100`:

```python
# gauche : 111 ^ ~100 & 111 = 111 ^ 011 & 111 = 100
# droite : 11001 & 111 = 001
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # false
    found = True
    break
```

Pour le deuxième comp_mask `010`:

```python
# gauche : 111 ^ ~010 & 111 = 111 ^ 101 & 111 = 010
# droite : 11001 & 111 = 001
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # false
    found = True
    break
```

Pour le troisième comp_mask `001`:

```python
# gauche : 111 ^ ~001 & 111 = 111 ^ 110 & 111 = 001
# droite : 11001 & 111 = 001
if (attempt ^ (~comp_mask)) & flip_mask == num & flip_mask: # true
    found = True
    break
```

> `25` est donc possible et doit être conservé. On voit d'ailleurs que la séquence de bits `11001` possède exactement 2 bits `0` et 1 bit `1` spécifiquement dans les positions ou il y a eut un flip, ce qui était notre condition initiale pour une permutation valide (voir [Condition sur les permutations](#Condition-sur-les-permutations)).

Finalement, on recommence le processus avec une nouvelle permutation dans celles restantes, jusqu'à ce qu'on trouve la combinaison secrète.
